
import{ BrowserRouter, Routes, Route} from "react-router-dom";
import Login from './component/Login';
import Dashboard from './component/Dashboard';
import Header from './component/Header';
import Frontpage from './component/Frontpage'
import Details from './component/Details'
import Home from './component/Home'
import Animedetails from "./component/Animedetails";
import Topviews from "./component/Dtopviews";
import Landscapeview from "./component/Landscapeview";
function App() {
  return (
    <BrowserRouter>
    <Routes>
      <Route path="/" Component={Home} />
      <Route path='/Login' Component={Login} />
      <Route path='/Dashboard' Component={Dashboard} />
      <Route path='/header' Component={Header} />
      <Route path="/Frontpage" Component={Frontpage} />
      <Route path="/Details" Component={Details} />
      <Route path="Animedetails" Component={Animedetails} />
      <Route path="Landscapeview" Component={Landscapeview} />
    </Routes>
    
    </BrowserRouter>
    
   
  
  );
}

export default App;
