import React, { useState, useEffect } from "react";
import "./topviews.css";

function Landscapeview() {
  const [tdata, Settdata] = useState();
  const getanimeapit = async () => {
    const animetdata = await fetch("https://api.jikan.moe/v4/anime");
    const reso = await animetdata.json();

    Settdata(reso.data);
  };
  useEffect(() => {
    getanimeapit();
  }, []);

  return (
    <div className="mainTop">
      <div className="headTop">
        <div className="redTop"></div>
        <div className="headingTop">TOP VIEWS</div>
        <div className="timeTop">
          <p>Day</p>
          <p>Week</p>
          <p>Month</p>
          <p>Year</p>
        </div>
      </div>
      <div className="cardTop">
        {tdata &&
          tdata.slice(6, 11).map((e, i) => {
            return (
              <div className="animetimg" key={i}>
                <img src={e.images && e.images.jpg.image_url} alt="cdcd" />
                <p>{e.titles && e.titles[0].title}</p>
                <div className="episode">{e.episodes}</div>
              </div>
            );
          })}
      </div>
    </div>
  );
}

export default Landscapeview;
