import React, { useEffect, useState } from "react";
import "./Dashboard.css";
import { BsArrowRight } from "react-icons/bs";
import { FaRegComments } from "react-icons/fa";
import imagedemon from "./demonslayer.webp";
import { useNavigate } from "react-router-dom";
import Details from "./Details";

function Dashboard() {
  const [data, Setdata] = useState();
  const [showComponent, setShowComponent] = useState(false);

  const nav = useNavigate();
  const getanimeapi = async () => {
    const animedata = await fetch("https://api.jikan.moe/v4/anime");
    const res = await animedata.json();

    Setdata(res.data);
  };
  useEffect(() => {
    getanimeapi();
  }, []);

  const animedetail = () => {
    return <Details />;
    // nav(`./Details/${x}` )

    console.log("hrgr");
  };

  return (
    <div className="dashmain">
      <div className="dashhead">
        <div className="dashhead1"></div>
        <h1 className="Dheadh1">TRENDING NOW</h1>
        <div className="dhead2">
          <p className="dashp">View All</p>
          <BsArrowRight className="dasharrow" />
        </div>
      </div>

      <div className="dashdata">
        {data &&
          data.slice(0, 6).map((e) => {
            return (
              <div
                className="dashcontent"
                onClick={() => {
                  animedetail();
                }}
              >
                <div className="dashcard">
                  <div className="dashimg">
                    <img
                      src={e.images.jpg.image_url}
                      alt="veve"
                      className="animeimg"
                    />
                  </div>
                  <div className="onimgdata">
                    <div className="episode">{e.episodes}</div>
                    <div className="comment">{e.score}⭐</div>
                    <div className="views">850</div>
                  </div>
                </div>
                <div className="carddetail">
                  <div className="cardgener">
                    <div className="actiongener">{e.genres[0].name}</div>
                    <div className="moviegener">{e.type}</div>
                  </div>
                  <div className="animename">{e.titles[0].title}</div>
                </div>
              </div>
            );
          })}
      </div>
    </div>
  );
}

export default Dashboard;
