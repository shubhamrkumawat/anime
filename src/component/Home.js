import React from 'react'
import './Home.css';
import Dashboard from './Dashboard';
import Header from './Header';
import Frontpage from './Frontpage'
// import Topviews from './Landscapeview';
import Landscapeview from './Landscapeview';

function Home () {
  return (
    <div>
      <Header/>
      <Frontpage/>
      <div className='Dashviews'>
        <Dashboard/>
      <Landscapeview />
      </div>
      
      
    </div>
  )
}

export default Home