import React from 'react'
import './animedetails.css'
import Header from './Header';
import Details from './Details';
import Landscapeview from './Landscapeview';
import Dashboard from './Dashboard';

function Animedetails() {
  return (
    <div className='andetails'><Header />
         <Details />
         <div className='Dashviews'>
        <Dashboard/>
      <Landscapeview />
      </div>
    </div>
  )
}

export default Animedetails