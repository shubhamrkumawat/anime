import React from 'react'
import './Header.css'
import { VscSearch } from 'react-icons/vsc';
import {MdAccountCircle } from 'react-icons/md';
import { Link } from 'react-router-dom';
const Header = () => {
   return (
    
<header className='HeadBox'>
    <div className='headNavBar'>
        <h1 className='hh1'>Anime</h1>
        <div className='list'>
            <ul className='Unli'>
                <li className='homepageli'>Homepage</li>
                <li className='category'>Categories
                <ul className='hdropdown'><li>anime details</li>
                <li>anime watching</li>
                <li>blog information</li>
                <li>sign up</li>
                <li>login in</li>
                    </ul></li>
                <li>Our Blog</li>
                <li>Contacts</li>
            </ul>
        </div>
        <div className="iconx">
        <VscSearch className='hicon'/>
        <Link to="./Login">  <MdAccountCircle
        className='hicon'/></Link>
      
        </div>
    </div></header>
  )
}

export default Header