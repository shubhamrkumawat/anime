import React from "react";
import "./details.css";
import image from "./demonslayer.webp";
import {} from "react-icons/";


function Details() {
  return (
    
    <div className="detmain">
      <div className="relpath">Home</div>
      <div className="detcontent">
        <dvi className="detcard">
          <img src={image} alt="" />
          <div className="detview">view</div>
          <div className="detcomment">comment</div>
        </dvi>
        <div className="datadet">
          <div className="dethead">
           
            <h1>Heading</h1>
            <p>hello</p>
          </div>
          <div className="animedetdata">
            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Accusamus
            adipisci hic iusto libero pariatur, nihil dolor? Totam, sed fugit
            saepe eveniet aut rem aperiam voluptatum, illum eos nulla,
            recusandae vero! Tenetur non a quidem obcaecati? Itaque optio facere
            quae tempora, et repudiandae eligendi consequuntur nemo aut, odio
            quos aliquid aliquam?
          </div>
          <div className="detcheck">
            <ul>
              <li>Type</li>
              <li>Studios</li>
              <li>Date Aired</li>
              <li>Status</li>
              <li>Genre</li>
            </ul>
            <ul>
              <li>Score</li>
              <li>Rating</li>
              <li>Duration</li>
              <li>Quality</li>
              <li>Views</li>
            </ul>
          </div>
          <div className="detbutton">
            <button className="fbut">FOLLOW</button>
            <button className="fbutwatch">WATCH NOW</button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Details;
