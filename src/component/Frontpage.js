import React from 'react'
import './frontpage.css'
import imagedemon from './demonslayer.webp'
import { MdKeyboardArrowRight } from 'react-icons/md';
import { MdKeyboardArrowLeft } from 'react-icons/md';
function Frontpage() {
  return (
    <div className='frontpage'>
        <div className="frontmain">
            <div className='butleft'><div className='secondbut'><MdKeyboardArrowLeft /></div></div>
           <img src={imagedemon} alt="fff" className='frontimg'/>
           <div className='butright'><div className='firstbut'><MdKeyboardArrowRight /></div></div>
           
        </div>
        
        
       
    </div>
    
  )
}

export default Frontpage