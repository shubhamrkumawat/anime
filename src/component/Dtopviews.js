import React, { useState, useEffect } from "react";
import "./topviews.css";

function Dtopviews() {
  const [tdata, Settdata] = useState();
  const getanimeapit = async () => {
    const animedata = await fetch("https://api.jikan.moe/v4/anime");
    const res = await animedata.json();

    Settdata(res.tdata);
  };
  useEffect(() => {
    getanimeapit();
  }, []);

  return (
    <div className="mainTop">
      <div className="headTop">
        <div className="redTop"></div>
        <div className="headingTop">TOP VIEWS</div>
        <div className="timeTop">
          <p>Day</p>
          <p>Week</p>
          <p>Month</p>
          <p>Year</p>
        </div>
      </div>
      <div className="cardTop">
        {tdata &&
          tdata.map((e) => {
            <div>
              <img src={e.images.jpg.image_url} alt="cdcd" />
            </div>;
            
          })}
      </div>
    </div>
  );
}

export default Dtopviews;
